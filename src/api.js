const API_KEY = "NWtC4g0gKLNY8bjlQMtHZGlVO4TStuIL";

export const searchByText = async ({ city }) => {
  const url = `locations/v1/cities/search?apikey=${API_KEY}&q=${city}`;
  const resp = await fetch(url);
  return resp.json();
};

export const searchByGeoposition = async ({ lat, lng }) => {
  const url = `locations/v1/cities/geoposition/search?apikey=${API_KEY}&q=${lat},${lng}`;
  const resp = await fetch(url);
  return resp.json();
};
export const getCurrentCondition = async ({ locationKey }) => {
  const url = `currentconditions/v1/${locationKey}?apikey=${API_KEY}`;
  const resp = await fetch(url);
  return resp.json();
};
export const getDailyForecast = async ({ locationKey }) => {
  const url = `forecasts/v1/daily/5day/${locationKey}?apikey=${API_KEY}`;
  const resp = await fetch(url);
  return resp.json();
};
export const getHourlyForecast = async ({ locationKey }) => {
  const url = `forecasts/v1/hourly/12hour/${locationKey}?apikey=${API_KEY}`;
  const resp = await fetch(url);
  return resp.json();
};

export const getIcons = async ({ iconNumber }) => {
  const url = `https://apidev.accuweather.com/developers/Media/Default/WeatherIcons/${
    iconNumber < 10 ? "0" + iconNumber : iconNumber
  }-s.png`;
  const resp = await fetch(url);
  return resp.json();
};
