export const dailyForecastExample = [
  {
    Date: "2021-08-01T07:00:00+02:00",
    EpochDate: 1627794000,
    Temperature: {
      Minimum: {
        Value: 57,
        Unit: "F",
        UnitType: 18,
      },
      Maximum: {
        Value: 72,
        Unit: "F",
        UnitType: 18,
      },
    },
    Day: {
      Icon: 17,
      IconPhrase: "Partly sunny w/ t-storms",
      HasPrecipitation: true,
      PrecipitationType: "Rain",
      PrecipitationIntensity: "Moderate",
    },
    Night: {
      Icon: 7,
      IconPhrase: "Cloudy",
      HasPrecipitation: false,
    },
    Sources: ["AccuWeather"],
    MobileLink:
      "http://www.accuweather.com/en/pl/koaczkow/268759/daily-weather-forecast/268759?day=1&lang=en-us",
    Link: "http://www.accuweather.com/en/pl/koaczkow/268759/daily-weather-forecast/268759?day=1&lang=en-us",
  },
  {
    Date: "2021-08-02T07:00:00+02:00",
    EpochDate: 1627880400,
    Temperature: {
      Minimum: {
        Value: 54,
        Unit: "F",
        UnitType: 18,
      },
      Maximum: {
        Value: 71,
        Unit: "F",
        UnitType: 18,
      },
    },
    Day: {
      Icon: 12,
      IconPhrase: "Showers",
      HasPrecipitation: true,
      PrecipitationType: "Rain",
      PrecipitationIntensity: "Light",
    },
    Night: {
      Icon: 34,
      IconPhrase: "Mostly clear",
      HasPrecipitation: false,
    },
    Sources: ["AccuWeather"],
    MobileLink:
      "http://www.accuweather.com/en/pl/koaczkow/268759/daily-weather-forecast/268759?day=2&lang=en-us",
    Link: "http://www.accuweather.com/en/pl/koaczkow/268759/daily-weather-forecast/268759?day=2&lang=en-us",
  },
  {
    Date: "2021-08-03T07:00:00+02:00",
    EpochDate: 1627966800,
    Temperature: {
      Minimum: {
        Value: 55,
        Unit: "F",
        UnitType: 18,
      },
      Maximum: {
        Value: 72,
        Unit: "F",
        UnitType: 18,
      },
    },
    Day: {
      Icon: 4,
      IconPhrase: "Intermittent clouds",
      HasPrecipitation: false,
    },
    Night: {
      Icon: 36,
      IconPhrase: "Intermittent clouds",
      HasPrecipitation: false,
    },
    Sources: ["AccuWeather"],
    MobileLink:
      "http://www.accuweather.com/en/pl/koaczkow/268759/daily-weather-forecast/268759?day=3&lang=en-us",
    Link: "http://www.accuweather.com/en/pl/koaczkow/268759/daily-weather-forecast/268759?day=3&lang=en-us",
  },
  {
    Date: "2021-08-04T07:00:00+02:00",
    EpochDate: 1628053200,
    Temperature: {
      Minimum: {
        Value: 58,
        Unit: "F",
        UnitType: 18,
      },
      Maximum: {
        Value: 73,
        Unit: "F",
        UnitType: 18,
      },
    },
    Day: {
      Icon: 4,
      IconPhrase: "Intermittent clouds",
      HasPrecipitation: false,
    },
    Night: {
      Icon: 35,
      IconPhrase: "Partly cloudy",
      HasPrecipitation: true,
      PrecipitationType: "Rain",
      PrecipitationIntensity: "Light",
    },
    Sources: ["AccuWeather"],
    MobileLink:
      "http://www.accuweather.com/en/pl/koaczkow/268759/daily-weather-forecast/268759?day=4&lang=en-us",
    Link: "http://www.accuweather.com/en/pl/koaczkow/268759/daily-weather-forecast/268759?day=4&lang=en-us",
  },
  {
    Date: "2021-08-05T07:00:00+02:00",
    EpochDate: 1628139600,
    Temperature: {
      Minimum: {
        Value: 57,
        Unit: "F",
        UnitType: 18,
      },
      Maximum: {
        Value: 71,
        Unit: "F",
        UnitType: 18,
      },
    },
    Day: {
      Icon: 4,
      IconPhrase: "Intermittent clouds",
      HasPrecipitation: false,
    },
    Night: {
      Icon: 34,
      IconPhrase: "Mostly clear",
      HasPrecipitation: false,
    },
    Sources: ["AccuWeather"],
    MobileLink:
      "http://www.accuweather.com/en/pl/koaczkow/268759/daily-weather-forecast/268759?day=5&lang=en-us",
    Link: "http://www.accuweather.com/en/pl/koaczkow/268759/daily-weather-forecast/268759?day=5&lang=en-us",
  },
];
