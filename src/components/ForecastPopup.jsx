import React, { memo, useEffect, useState } from "react";
import { Popup } from "react-leaflet";
import {
  ButtonGroup,
  Card,
  Col,
  Container,
  //   ListGroup,
  Row,
  Spinner,
} from "react-bootstrap";
import styled from "styled-components";
import { Button } from "react-bootstrap";
import { ChevronLeft, ChevronRight } from "react-bootstrap-icons";
import { useRef } from "react";

const formatDate = (dateString) => {
  let date = new Date(dateString);

  /*   const hours = date.getHours().toString();
  const minutes = date.getMinutes().toString();
  const time = `${hours < 10 ? "0" + hours : hours}:${
    minutes < 10 ? "0" + minutes : minutes
  }`; */

  const day = date.getDate().toString();
  /*   const month = (date.getMonth() + 1).toString();
  const year = date.getFullYear().toString(); */

  const weekday = date.toLocaleString("default", { weekday: "short" });
  const monthName = date.toLocaleString("default", { month: "short" });
  date = `${
    weekday.charAt(0).toUpperCase() + weekday.slice(1)
  } ${day} ${monthName}`;
  return date;
};
const toCelsius = (fahrenheit) => {
  return Math.round((fahrenheit - 32) * (5 / 9));
};

const CustomPopup = styled(Popup)`
  .leaflet-popup-content {
    min-width: 25em;
  }
  .leaflet-popup-content-wrapper {
    background-color: rgba(54, 103, 132, 0.8);
  }
  .leaflet-popup-content-wrapper,
  .leaflet-popup-tip {
    background-color: rgba(54, 103, 132, 0.8);
  }
`;

const NavButtonGroup = styled.div`
  display: flex;
  justify-content: space-between;
  width: 100%;
`;

const NavButton = styled(Button)`
  border-radius: 5em;
`;
const LeftIcon = ChevronLeft;

const RightIcon = ChevronRight;

const ForecastPopup = ({ position, forecasts }) => {
  const [selectedForecastDay, setSelectedForecastDay] = useState(0);
  const [disabledNextButton, setDisabledNextButton] = useState(false);
  const [disabledPrevButton, setDisabledPrevButton] = useState(true);
  const [forecastDaytime, setForecastDaytime] = useState("Day");

  const dayButtonRef = useRef();
  const nightButtonRef = useRef();

  useEffect(() => {
    if (selectedForecastDay === forecasts.length - 1) {
      setDisabledNextButton(true);
    } else if (selectedForecastDay === 0) {
      setDisabledPrevButton(true);
    } else {
      setDisabledPrevButton(false);
      setDisabledNextButton(false);
    }
  }, [selectedForecastDay, forecasts]);

  const handlePrev = () => {
    setSelectedForecastDay(selectedForecastDay - 1);
    /* if (selectedForecastDay === 0) {
      setDisabledPrevButton(true);
    }
    if (selectedForecastDay === forecasts.length) {
        setDisabledNextButton(false);
    } */
  };

  const handleNext = () => {
    setSelectedForecastDay(selectedForecastDay + 1);
    /* if (selectedForecastDay + 1=== forecasts.length) {
      setDisabledNextButton(true);
    }
    if (selectedForecastDay === 0) {
      setDisabledPrevButton(true);
    } */
  };
  const handleDayClick = () => {
    setForecastDaytime("Day");
    console.log(dayButtonRef.current);
    dayButtonRef.current.style.backgroundColor = "rgba(59,111,142,1)";
    dayButtonRef.current.style.color = "white";
    nightButtonRef.current.style.backgroundColor = "#f8f9fa";
    nightButtonRef.current.style.color = "black";
  };
  const handleNightClick = () => {
    setForecastDaytime("Night");
    nightButtonRef.current.style.backgroundColor = "rgba(59,111,142,1)";
    nightButtonRef.current.style.color = "white";
    dayButtonRef.current.style.backgroundColor = "#f8f9fa";
    dayButtonRef.current.style.color = "black";
  };
  return (
    <CustomPopup keepInView={true} offset={[0, -30]} position={position}>
      {forecasts.length > 0 ? (
        <div>
          <NavButtonGroup>
            <NavButton
              variant="light"
              onClick={handlePrev}
              disabled={disabledPrevButton}
            >
              <LeftIcon />
            </NavButton>
            <ButtonGroup>
              <Button
                variant="light"
                onClick={handleDayClick}
                ref={dayButtonRef}
              >
                Day
              </Button>
              <Button
                variant="light"
                onClick={handleNightClick}
                ref={nightButtonRef}
              >
                Night
              </Button>
            </ButtonGroup>
            <NavButton
              variant="light"
              onClick={handleNext}
              disabled={disabledNextButton}
            >
              <RightIcon />
            </NavButton>
          </NavButtonGroup>
          <Card>
            <Container>
              <Row xs={1} md={1} className="justify-content-md-center">
                <Col md="auto">
                  <Card.Title>
                    {formatDate(forecasts[selectedForecastDay].Date)}
                  </Card.Title>
                </Col>
              </Row>
              <Row>
                <Col>
                  <img
                    src={`https://apidev.accuweather.com/developers/Media/Default/WeatherIcons/${
                      forecasts[selectedForecastDay][forecastDaytime].Icon < 10
                        ? "0" +
                          forecasts[selectedForecastDay][forecastDaytime].Icon
                        : forecasts[selectedForecastDay][forecastDaytime].Icon
                    }-s.png`}
                    alt="forecast icon"
                  />
                </Col>
                <Col>
                  <Card.Subtitle>
                    <span style={{ color: "gray", opacity: 0.6, fontSize: 12 }}>
                      max{" "}
                    </span>
                    {toCelsius(
                      forecasts[selectedForecastDay].Temperature.Maximum.Value
                    )}
                    &deg;C
                    <br />
                    <span style={{ color: "gray" }}>
                      <span
                        style={{ color: "gray", opacity: 0.6, fontSize: 12 }}
                      >
                        min{" "}
                      </span>{" "}
                      {toCelsius(
                        forecasts[selectedForecastDay].Temperature.Minimum.Value
                      )}
                      &deg;C
                    </span>
                  </Card.Subtitle>
                </Col>
                <Col>
                  <Card.Text>
                    {forecasts[selectedForecastDay][forecastDaytime].IconPhrase}
                  </Card.Text>
                </Col>
              </Row>
            </Container>
          </Card>
          {/* <Card>
            <ListGroup variant="flush">
              {forecasts.slice(1, forecasts.length).map((forecast, idx) => (
                <ListGroup.Item key={idx}>
                  {formatDate(forecast.Date)}
                </ListGroup.Item>
              ))}
            </ListGroup>
          </Card> */}
        </div>
      ) : (
        <div style={{ textAlign: "center" }}>
          <Spinner animation="border" variant="light" />
          <p style={{ color: "white" }}>Loading...</p>
        </div>
      )}
    </CustomPopup>
  );
};

export default memo(ForecastPopup);
