import {
  useLayoutEffect,
  useEffect,
  useState,
  useRef,
  memo,
  //   lazy,
  Suspense,
} from "react";
import { Marker, useMapEvents } from "react-leaflet";
import {
  getDailyForecast,
  getHourlyForecast,
  searchByGeoposition,
  searchByText,
} from "../api";
import ForecastPopup from "./ForecastPopup";
import { dailyForecastExample } from "../data/sampleForecast";
import { Spinner } from "react-bootstrap";

// const { dailyForecastExample } = lazy(() => import("../data/sampleForecast"));
const CustomMarker = ({ innerRef }) => {
  const [dailyForecasts, setDailyForecasts] = useState([]);
  // const [hourlyForecasts, setHourlyForecasts] = useState([]);
  // const markerRef = useRef(null);
  const [selectedLocalization, setSelectedLocalization] = useState({});
  const [markerCoords, setMarkerCoords] = useState(null);
  const [locationKey, setLocationKey] = useState();

  const popupRef = useRef(null);

  useLayoutEffect(() => {
    if (locationKey) {
      let newDailyForecasts = [];
      let newHourlyForecasts = [];
      setDailyForecasts(dailyForecastExample);
      /* getDailyForecast({
        locationKey,
      })
        .then((response) => {
          console.log(response.DailyForecasts);
          newDailyForecasts = response.DailyForecasts;
          setDailyForecasts(response.DailyForecasts);
          return (
            <>
              {" "}
              <Marker position={markerCoords}></Marker>
              <ForecastPopup
                position={markerCoords}
                forecasts={dailyForecasts}
              />
            </>
          );
        })
        .catch((err) => {
          console.log(err);
        }); */
      /* getHourlyForecast({
          locationKey,
        })
          .then((response) => {
            console.log(response);
            newHourlyForecasts = response;
            setHourlyForecasts(response);
          })
          .catch((err) => {
            console.log(err);
          }); */
    }
  }, [locationKey]);
  /* useEffect(() => {
    return (
      <>
        {" "}
        <Marker position={markerCoords}></Marker>
        <ForecastPopup position={markerCoords} forecasts={dailyForecasts} />
      </>
    );
  }, [dailyForecasts]); */
  const map = useMapEvents({
    click(e) {
      setMarkerCoords(e.latlng);
      if (dailyForecasts.length > 0) {
        setDailyForecasts([]);
      }
      console.log(e.latlng);
      if (popupRef.current) {
        innerRef.current.openPopup(popupRef.current);
      }
      searchByGeoposition(e.latlng).then((response) => {
        console.log(response);
        console.log(response.Key);

        setLocationKey(response.Key);
        setSelectedLocalization(response);
      });
    },
  });
  return markerCoords === null ? null : (
    <Suspense fallback={<Spinner animation="border" />}>
      {" "}
      <Marker position={markerCoords}></Marker>
      <ForecastPopup position={markerCoords} forecasts={dailyForecasts} />
    </Suspense>
  );
};
export default memo(CustomMarker);
