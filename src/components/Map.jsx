import React from "react";
import { MapContainer, TileLayer } from "react-leaflet";
import { createRef } from "react";
import CustomMarker from "./CustomMarker";

const Map = () => {
  const mapRef = createRef();

  return (
    <div>
      {/* <ForecastPanel /> */}
      <MapContainer
        innerRef={mapRef}
        center={[52.335339, 19.178398]}
        zoom={7}
        style={{
          position: "absolute",
          top: 0,
          bottom: 0,
          width: "100%",
        }}
        whenCreated={(map) => (mapRef.current = map)}
      >
        <TileLayer
          attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        />
        <CustomMarker forwardedRef={mapRef} />
        {/* <ForecastPanel show={showForecastPanel} onHide={handleClose} /> */}
        {
          // TODO: zrobić panel wysuwany z boku z prognozą w klikniętym miejscu
          // (użytkownik może zmienić szerokość)
        }
      </MapContainer>
    </div>
  );
};

export default React.memo(Map);
