import { Offcanvas } from "react-bootstrap";
import React from "react";
import styled from "styled-components";

const ForecastPanel = (props) => {
  const { show, onHide } = props;
  
  return (
    <Offcanvas show={show} onHide={onHide}>
      <Offcanvas.Header closeButton>
        <Offcanvas.Title>Offcanvas</Offcanvas.Title>
      </Offcanvas.Header>
      <Offcanvas.Body>
        Some text as placeholder. In real life you can have the elements you
        have chosen. Like, text, images, lists, etc.
      </Offcanvas.Body>
    </Offcanvas>
  );
};

export default ForecastPanel;
